Donat l'anterior exercici partirem a realitzar el primer repte. Una mecànica i Gameplay senzill però complert.

Resum del joc: la nostra personatge ha d'esquivar el màxim de boles de foc 🔥 que cauen del cel i esclaten al terra. Si una bola la toca, ella mort i acaba la partida. Si la personatge cau per un dels extrems del terra, acaba la partida. El joc té un comptador de temps enrere. Si el rellotge arriba a 0, s'acaba el joc. La condició de victòria es donarà segons el mode de partida escollit.

Punts a complir del repte:
- Mode de joc 1: el jugador mou a la guerrera. El joc generarà aleatòriament boles de foc que cauran del cel al terra. 
        - Condicions de derrota:
            - Si el jugador cau per un dels extrems del terra perd.
-Si la protagonista és copejada per una bola de foc.
        - Condicions de victòria:
 - Si el jugador aconsegueix esquivar totes les boles del joc durant un temps determinat, el jugador guanya.
            - El rellotge arriba a 0 i la Guerrera continua viva.
    Elements i punts a desenvolupar:
    - Guerrera:
        - Té una sola vida: si toca la bola de foc el joc acaba. El jugador ha perdut.
        - Animacions:
            - Idle: no es mou, estat quan no es fa cap interacció.
            - Walk: animació de caminar que es realitza al desplaçar-se
            - (Bonus) Jump
            - Lose: animació que s'activa amb la derrota per l'impacte amb una bola.
        - Controls, el jugador té les següents accions (verbs) de la Guerrera:
            - Moviment lateral amb les tecles del teclat: fletxes o "A" / "D"
            - Salt: amb la tecla "espai" el personatge salta. El salt es realitza mantenint la direcció horitzontal queja portava en el moment previ. No hi ha doble ni triple salt. Durant el salt, la tecla "espai" no tindrà més efecte. No es pot saltar si el personatge no toca terra.
    - Bola de foc:
        - Cicle de vida:
            - Es genera aleatòriament en una posició X a una certa alçada del terra.
            - Cau en línia recta (moviment vertical) cap al terra
            - A l'impactar esclata i desapareix. Pot impactar contra el terra o contra la Guerrera.
                - (Bonus) Força d'explosió: si impacta a prop de la Guerrera emet una força amb la que empeny al personatge en direcció oposada al punt de l'explosió.
        - Animacions de la bola de foc:
            - Idle/Caiguda: mentres cau.

            - Impacte: la bola de foc desapareix al impactar                -  (Bonus) Efecte de destrucció: animació de com es destrueix la bola de foc. 
                - (Bonus) Moment d'impacte: la bola de foc té la capacitat d'eliminar a la Guerrera mentres no col·lisiona amb el terra. Un cop col·lisiona amb el terra i durant l'animació d'explosió no eliminarà al personatge.
       
    - Control de la aleatorietat: crear l'objecte que gestiona la generació aleatòria de boles de foc. Haurà de tenir en compte la freqüència de boles de foc i la posició on es genera cadascuna. 
        - Es tindrà en compte l'ús correcte de Prefabs, estratègies, definició de variables i funcions especifiques explicades a classe.



    - (Bonus) Xampinyó verinós:
        - Resum: aleatòriament en comptes de generar-se una bola de foc es generarà un xampinyó verinós. Aquesta aleatorietat ha de ser molt baixa.
        - Cicle de vida:
            - Es genera aleatòriament en X i a la mateixa alçada que la bola de foc.
            - Cau en línia recta (moviment vertical) cap al terra.
            - A l'impactar al terra estarà allà durant un valor "t" de temps. Quan acaba aquest temps, si no ha impactat amb la guerrera, desapareix (destroy).
            - A l'impactar o tocar la Guerrera la enverina activant l'habilitat gegantina d'aquesta. Fent més difícil esquivar les boles de foc.
        - Animacions:
            - Idle/Caiguda: mentres cau.
                - (Bonus) Animació de parpellejar moments abans de desaparèixer.

            - Impacte amb el personatge: el bolet desapareix
                -  (Bonus) Efecte de destrucció: animació de com es destrueix el xampinyó

- Mode de joc 2: el jugador controla on es generen les boles de foc. El Guerrer correrà d'un extrem a un altre i haurà de saber quan canviar de direcció de manera autònoma.
        - Condicions de victòria: 
            - Si el jugador aconsegueix que una bola de foc impacti amb la guerrera , el jugador guanya.
        - Condicions de derrota:
            - El rellotge arriba a 0 i el Guerrer continua viu.

    Elements i punts a desenvolupar:
    - Guerrer:
        - Té una sola vida: si toca la bola de foc el joc acaba. El jugador ha guanyat.
        - Animacions:
            - Idle: no es mou, estat quan no es fa cap interacció
            - Walk: animació de caminar que es realitza al desplaçar-se
            - Lose: animació que s'activa amb la derrota d'impacte amb una bola.
     -Accions de l'enemic:
  - Moviment: el moviment ha de ser horitzontal. Moviment autònom:
            - A l'arribar a la vora del terra ha de girar i continuar. S'ha de fer servir Ray i derivats.
            - Velocitat continua
            - (Bonus) Esquivar boles que van per davant: realitzar un mode en que detecti les boles que cauran davant del Guerrer i en aquell moment dona mitja volta.

    - Bola de foc:
        - Cicle de vida:
            -  Creació de la bola de foc: 
                - L'usuari generarà una bola de foc amb el botó esquerra del ratolí.
                - La bola es crearà a la posició en X (horitzontal) on s'ha fet clic.
                - L'alçada respecte el terra on es generen les boles serà sempre la mateixa.
-(Bonus) Cooldown que faci que després de fer un click, Fer un altre no spawneji una bola de foc fins que no pasi un cert temps.
            - Moviment: Cau en línia recta (moviment vertical) cap al terra
            - Al impactar esclata i desapareix. Pot impactar contra el terra o contra la Guerrera.                 - (Bonus) Moment d'impacte: la bola de foc te la capacitat d'eliminar a la Guerrera mentres no col·lisiona amb el terra. Un cop col·lisiona amb el terra i durant l'animació d'explosió no eliminarà al personatge.
                - (Bonus) Força d'explosió: força amb la que empeny al personatge en direcció oposada a l'explosió.
        - Animacions de la bola de foc:
            - Idle/Caiguda: mentres cau la bola en direcció al terra.
            - Impacte: la bola desapareix al impactar 
                -  (Bonus) Efecte de destrucció: animació de com es destrueix la bola de foc.               
    - (Bonus) Xampinyó verinós:
        - Resum: dels clics fets per l'usuari aleatòriament es generarà un xampinyó.
        - Cicle de vida:
            - Es genera aleatòriament en X a la mateixa alçada del terra que la bola de foc.
            - Cau en línia recta (moviment vertical) cap al terra
            - Al impactar al terra estarà allà durant un valor "t" de temps. Al acabar aquest temps no impacta amb la Guerrera desapareix (destroy).
            - Al impactar o tocar al Guerrer activarà un temps on el Guerrer no pot patir mal pel impacte directe de les boles de foc.
        - Animacions:
            - Idle/Caiguda: mentres cau.
                - Animació de parpellejar moments abans de desaparèixer.
            - Impacte: el bolet desapareix al impactar amb la guerrera
                - Efecte de destrucció: animació de com es destrueix el xampinyó

Altres elements de l'aplicació:

    - Pantalles:
    

    - StartScreen: Pantalla amb una UI amb
    

    

    - Títol del joc (a la vostre elecció)
    

    

    - Input text: per escriure el nom de la Guerrera
    

    

    - Botons de modus de joc: dos botons on escollir quin mode de joc és jugar.
    

    

    - Altres elements que sumin al vostre disseny
    

    - HUD: UI de joc. 
    

    

    - Comptador de temps enrere.              - Altres elements que sumin al vostre joc
         - GameOverScreen (Final de partida):
             - Títol anunciant victòria o derrota amb el nom que l'usuari ha donat a la Guerrera.
             - Botó de tornar a comença la partida
             - Botó per anar a la StartScreen.
              - Altres elements que sumin al vostre disseny
             - (Bonus) Número de boles de foc generades durant la partida.
   
 - GameManager: Creació de mínim un sol Manager que gestiona la partida. Amb funcions i variables que consulten o fan servir la resta d'elements de l'aplicació. La creació i ús de més managers diferents ha d'estar justificada.

/*****Fi definició del joc******/


A destacar:
    - Es valorarà la definició de noms de variables i funcions, ús coherent de components, estructura del codi, utilització adequada del git.
    - Es valorarà com a extra els elements i les aportacions que sumin al gamefeel i a l'estètica del joc. És prioritari la funcionalitat dels punts -no bonus- per sobre dels extres.
    - Els bonus repetits computen 1 sola vegada.
 
Format d'entrega:
    - Entrega de la url de gitlab del projecte.
    - Nom del projecte de Git: M17UF1R1-Cognom1Congnom2Nom
    - Projecte de git amb dos branques com a mínim:
        - Main (o Master): branca on es corregirà l'exercici en l'últim commit. Aquest es considera entrega, si està fora de termini rebrà penalització habitual.
        - Dev: branca de desenvolupament, commits de guardat durant el desenvolupament del repte. D'aquí han de sortir la resta de branques de treball si es necessari.