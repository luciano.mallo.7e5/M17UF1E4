using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    private Button _button;
    private GameObject _playerNameGO;
    private GameObject _playerSpeedGO;
    private GameObject _playerHeightGO;
    private GameObject _playerDistanceGO;
    private GameObject _playerTypeGO;
    private string _playerName;
    private float _playerSpeed;
    private float _playerHeight;
    private float _playerDistance;
    private string _playerType;



    // Start is called before the first frame update
    void Start()
    {
       
        _button = GameObject.Find("StartButton").GetComponent<Button>();

        _button.onClick.AddListener(SaveStatsOfCharacter);
    }

    private void SaveStatsOfCharacter(){
 
        _playerNameGO = GameObject.Find("Name");
       
        _playerDistanceGO = GameObject.Find("Distance");
        _playerHeightGO = GameObject.Find("Height");
        _playerSpeedGO = GameObject.Find("Speed");


        _playerName = _playerNameGO.GetComponent<Text>().text;
       
        if (float.TryParse(_playerDistanceGO.GetComponent<Text>().text, out _playerDistance))
        {
            _playerDistance = float.Parse(_playerDistanceGO.GetComponent<Text>().text);
        }
        else { 
            _playerDistance = 5f; 
        }

        if (float.TryParse(_playerHeightGO.GetComponent<Text>().text, out _playerHeight))
        {
            _playerHeight = float.Parse(_playerHeightGO.GetComponent<Text>().text);
        }
        else
        {
            _playerHeight = 5;
        }
        if (float.TryParse(_playerSpeedGO.GetComponent<Text>().text, out _playerSpeed))
        {
            _playerSpeed = float.Parse(_playerSpeedGO.GetComponent<Text>().text);
        }
        else 
        {
            _playerSpeed = 2f;
        }
        
        
       
       


        Controler.PlayerName = _playerName;
        
        Controler.PlayerDistance = _playerDistance;
        Controler.PlayerHeight = _playerHeight;
        Controler.PlayerSpeed = _playerSpeed;


        SceneManager.LoadScene("Game");
    }
 


    // Update is called once per frame
    void Update()
    {
        
    }
}
