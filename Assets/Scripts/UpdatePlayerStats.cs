using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdatePlayerStats : MonoBehaviour
{
    private GameObject _player;
    private PlayerData _playerData;
    private GameObject _playerSpeedGO;
    private GameObject _playerHeightGO;
    private GameObject _playerDistanceGO;
    private float _playerSpeed;
    private float _playerHeight;
    private float _playerDistance;


    // Start is called before the first frame update
    void Start()
    {

        _playerDistanceGO = GameObject.Find("Distance");
        _playerHeightGO = GameObject.Find("Height");
        _playerSpeedGO = GameObject.Find("Speed");

    }

    // Update is called once per frame
    void Update()
    {

        if (float.TryParse(_playerDistanceGO.GetComponent<Text>().text, out _playerDistance))
        {
            _playerDistance = float.Parse(_playerDistanceGO.GetComponent<Text>().text);
        }
        else
        {
            _playerDistance = 5f;
        }

        if (float.TryParse(_playerHeightGO.GetComponent<Text>().text, out _playerHeight))
        {
            _playerHeight = float.Parse(_playerHeightGO.GetComponent<Text>().text);
        }
        else
        {
            _playerHeight = 5;
        }
        if (float.TryParse(_playerSpeedGO.GetComponent<Text>().text, out _playerSpeed))
        {
            _playerSpeed = float.Parse(_playerSpeedGO.GetComponent<Text>().text);
        }
        else
        {
            _playerSpeed = 2f;
        }

        _player = GameObject.Find("Guard");
        _playerData = GameObject.Find("Guard").GetComponent<PlayerData>();

       _playerData.Speed =   _playerSpeed;
        _playerData.Height = _playerHeight;
        _playerData.DistanceTraveled = _playerDistance;

}
}
