using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

    private float _speedAtenuator = 0.01f; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
            transform.position += new Vector3(_speedAtenuator*Input.GetAxis("Horizontal"), 0, 0);
        
    }
}
