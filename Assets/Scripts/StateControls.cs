using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateControls : MonoBehaviour
{
    private float _backToNormalTime = 10f;
    private float _giantTime = 0f;
    private PlayerData _playerData;

    // Start is called before the first frame update
    void Start()
    {
        _playerData = GameObject.Find("Guard").GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            _playerData.PlayerState = GameStates.Giant;
            Debug.Log("pressed");
            Debug.Log(_playerData.PlayerState);
        }

        if (_playerData.PlayerState == GameStates.Giant) 
        { 
            if (Time.time < _giantTime)
                _giantTime = Time.time + _backToNormalTime;
                _playerData.PlayerState = GameStates.Normal; 
        }
        

    }
}
